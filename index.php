<?php

/**
 * The plugin bootstrap file
 *
 *
 * @link              https://gitlab.com/zodman/wp-auditmylink
 * @since             1.0.0
 * @package           auditmylink
 *
 * @wordpress-plugin
 * Plugin Name:       auditmylink
 * Plugin URI:        https://gitlab.com/zodman/wp-auditmylink
 * Description:       Coding shortcodes for auditmy.link
 * Version:           1.0.0
 * Author:            Andres Vargas
 * Author URI:        https://gitlab.com/zodman/wp-auditmylink
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       auditmylink
 * Domain Path:       /languages
 */



if( ! function_exists('auditmylink') ){
    add_action('plugins_loaded', function() {
        add_shortcode("auditmylink", "auditmylink");
    });

    function auditmylink($attr =[], $content=""){
        $content = trim($content);
        $attr = shortcode_atts(
            array(
                'id' => '1',
            ), $attr);
        $url = "http://app.auditmy.link/a/${attr['id']}/?url=${content}";
        $resp = wp_remote_get($url, array('redirection'=>0));
        return $resp["headers"]["Location"];

    }
}
